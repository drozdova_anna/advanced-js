const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж i м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

const wrapper = document.getElementById("root");
const ul = document.createElement("ul");
ul.classList.add("list");
wrapper.prepend(ul);

let counter = 1;

books.forEach( elem => {
    try{
        if ( elem.author && elem.name && elem.price ){
            const li = document.createElement("li");
            li.classList.add("list-elem");
            li.innerHTML = `author: ${elem.author}, name: ${elem.name}, price: ${elem.price}`;
            ul.prepend(li);
        } else {
            throw new Error();
        }
    }
    catch (e) {
        let missingProperty = "";
        if ( !elem.hasOwnProperty('author') ) missingProperty = "autor";
        else if ( !elem.hasOwnProperty('name') ) missingProperty = "name";
        else missingProperty = "price";

        console.log(`The element number ${counter} is missing a property ${missingProperty}`);
    }
    counter++;
})