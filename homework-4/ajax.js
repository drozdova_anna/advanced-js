const filmAPI = 'https://ajax.test-danit.com/api/swapi/films';
const filmAndCharacters = [];

const renderFilm = ( filmAndCharacters ) => {
  fetch(`${filmAPI}`)
    .then(response => response.json())
    .then(data => {
      const allFilmsWrapper = document.querySelector(".allFilmsWrapper");

      data.forEach( ({episodeId, name, openingCrawl, characters}) => {
          const filmWrapper = document.createElement("div");
          filmWrapper.classList.add("film");
          filmWrapper.innerHTML = `
            <div>
              <p>Episode: ${episodeId}</p>
              <p>Film: ${name}</p>
              <p>Summary: </p>
              <p>${openingCrawl}</p>
              <p>Characters: </p>
            </div>
            `;
          allFilmsWrapper.append(filmWrapper);
          
          const filmAndChar = {
            filmWrapper,
            characters
          }
          filmAndCharacters.push(filmAndChar);
      })
      return filmAndCharacters;
    })
    .then( filmAndCharacters => {
      filmAndCharacters.forEach( (elem) => {
        elem.characters.forEach( (charAPI) => {
          fetch(`${charAPI}`)
          .then(response => response.json())
          .then(({name}) => {
            elem.filmWrapper.innerHTML += `${name}, `;
          })
          .catch(() => {
            alert(`Data is missing`)
          })
        })
  
      })
    })
    .catch(() => {
      alert(`Data is missing`)
    })
  
}


renderFilm( filmAndCharacters );

