class Employee {
    constructor(name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    getName(){
        return this.name;
    }
    setName( name ){
        this.name = name;
    }
    getAge(){
        return this.age;
    }
    setAge( age ){
        this.age = age;
    }
    getSalary(){
        return this.salary;
    }
    setSalary( salary ){
        this.salary = salary;
    }
}

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }
    getSalary(){
        return this.salary * 3;
    }
}

const progAnia = new Programmer ("Ania", 22, 56000, "JS");
console.log(progAnia);

const progKatia = new Programmer ("Katia", 28, 78000, "C++");
console.log(progKatia);

const progOlia = new Programmer ("Olia", 45, 24000, "HTML");
console.log(progOlia);