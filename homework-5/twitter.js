
class Card {
    constructor(id, userId, title, body, name, userName, email) {
      this.id = id;
      this.userId = userId;
      this.title = title;
      this.body = body;
      this.name = name;
      this.userName = userName;
      this.email = email;
      this.card = document.createElement("div");
      this.del = document.createElement("button");
    }
    createElement() {
      this.card.classList.add("card");
      this.card.innerHTML = `
            <div class="name">Name: ${this.name}</div>
            <div class="email">Email: ${this.email}</div>
            <h3 class="title">Title: ${this.title}</h3>
            <h5 class="desc">${this.body}</h5>
      `
      this.del.innerHTML = "Delete post";
      this.del.addEventListener("click", () => {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
          method: "DELETE",
        })
          .then((res) => {
            if (res.ok) {
              this.card.remove();
            } else {
              throw new Error();
            }
          })
          .catch((error) => console.warn(error.message));
        })
      this.card.insertAdjacentElement('beforeend', this.del);
    }
    render(container) {
      this.createElement();
      return container.append(this.card);
    }  
  }
  

const container = document.querySelector(".container");

Promise.all([
  fetch("https://ajax.test-danit.com/api/json/users").then((res) =>
    res.json().then((data) => data)
  ),
  fetch("https://ajax.test-danit.com/api/json/posts").then((res) =>
    res.json().then((data) => data)
  ),
]).then((data) => {
  const [userArr, postsArr] = data;
  postsArr.forEach(({ id, userId, title, body }) => {
    const user = userArr.find((item) => item.id === userId);
    const card = new Card(id, userId, title, body, user.name, user.userName, user.email);
    card.render(container);
  });

  });

