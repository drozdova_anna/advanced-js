async function findPerson  (wrapper) {
    let IPresponse = await fetch('https://api.ipify.org/?format=json');
    let IP = await IPresponse.json();
    const {ip} = IP;

    let fizAdrResponse = await fetch(`http://ip-api.com/json/${ip}`);
    let fizAdr = await fizAdrResponse.json();

    const { continent, country, region, city, district } = fizAdr;
    wrapper.innerHTML = `
    <p>continent: ${continent} </p>
    <p>country: ${country} </p>
    <p>region: ${region} </p>
    <p>city: ${city} </p>
    <p>district: ${district} </p>
    `
}

const btn = document.querySelector(".searchIPbtn");
const info = document.querySelector(".info");

btn.addEventListener("click", () => {
    findPerson(info);
})